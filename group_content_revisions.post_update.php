<?php

/**
 * @file
 * Post update functions for Group Content Revisions module.
 */

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\group_content_revisions\RevisionableGroupContentHelper;

/**
 * Update group content entities to be revisionable.
 */
function group_content_revisions_post_update_make_group_content_revisionable(&$sandbox) {
  $definition_update_manager = \Drupal::entityDefinitionUpdateManager();
  /** @var \Drupal\Core\Entity\EntityLastInstalledSchemaRepositoryInterface $last_installed_schema_repository */
  $last_installed_schema_repository = \Drupal::service('entity.last_installed_schema.repository');

  $entity_type = $definition_update_manager->getEntityType('group_content');
  $field_storage_definitions = $last_installed_schema_repository
    ->getLastInstalledFieldStorageDefinitions('group_content');

  // Update the entity type definition.
  $entity_keys = $entity_type->getKeys();
  $entity_keys['revision'] = 'vid';
  $entity_keys['revision_translation_affected'] = 'revision_translation_affected';

  $entity_type->set('entity_keys', $entity_keys);
  $entity_type->set('revision_table', 'group_content_revision');
  $entity_type->set('revision_data_table', 'group_content_field_revision');
  $entity_type->set('show_revision_ui', TRUE);

  $revision_metadata_keys = [
    'revision_user' => 'revision_uid',
    'revision_created' => 'revision_timestamp',
    'revision_log_message' => 'revision_log',
    'revision_default' => 'revision_default',
  ];

  $entity_type->set('revision_metadata_keys', $revision_metadata_keys);

  // Update the field storage definitions and add the new ones required by a
  // revisionable entity type.
  $field_storage_definitions['langcode']->setRevisionable(TRUE);
  $field_storage_definitions['label']->setRevisionable(TRUE);
  $field_storage_definitions['changed']->setRevisionable(TRUE);

  $helper = new RevisionableGroupContentHelper('group_content');
  $revision_definitions = $helper->baseFieldDefinitions();

  array_merge($field_storage_definitions, $revision_definitions);

  $definition_update_manager->updateFieldableEntityType($entity_type, $field_storage_definitions, $sandbox);

  return new TranslatableMarkup('Group content entities have been converted to be revisionable.');
}
