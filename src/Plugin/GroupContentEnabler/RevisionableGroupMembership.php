<?php

namespace Drupal\group_content_revisions\Plugin\GroupContentEnabler;

use Drupal\group\Plugin\GroupContentEnabler\GroupMembership;

/**
 * Overrides to group_membership plugin to support revisions.
 */
class RevisionableGroupMembership extends GroupMembership {

  use RevisionableGroupContentEnablerTrait;

}
