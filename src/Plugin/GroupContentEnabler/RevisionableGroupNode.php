<?php

namespace Drupal\group_content_revisions\Plugin\GroupContentEnabler;

use Drupal\gnode\Plugin\GroupContentEnabler\GroupNode;

/**
 * Overrides to group_node plugin to support revisions.
 */
class RevisionableGroupNode extends GroupNode {

  use RevisionableGroupContentEnablerTrait;

}
