<?php

namespace Drupal\group_content_revisions\Plugin\GroupContentEnabler;

use Drupal\Core\Form\FormStateInterface;

/**
 * Overrides to group plugin methods to support revisions.
 */
trait RevisionableGroupContentEnablerTrait {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config['new_revision'] = FALSE;

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['new_revision'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Create new revision'),
      '#default_value' => $this->configuration['new_revision'],
      '#description' => $this->t("Enable revisioning for this group content type."),
    ];

    return $form;
  }

}
