<?php

namespace Drupal\group_content_revisions\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Provides a base class for entity decorators to extend.
 *
 * @see https://en.wikipedia.org/wiki/Decorator_pattern
 * @see https://www.drupal.org/sandbox/solomonrothman/2977603
 *
 * @ingroup entity_api
 */
abstract class EntityDecorator implements EntityInterface {
  /**
   * The decorated entity.
   *
   * @var \Drupal\Core\Entity\EntityInterface
   */
  protected $subject;

  /**
   * EntityDecorator constructor.
   *
   * @param \Drupal\Core\Entity\EntityInterface $subject
   *   The entity being decorated.
   */
  public function __construct(EntityInterface $subject) {
    $this->subject = $subject;
  }

  /**
   * Passes through all unknown calls onto the subject.
   */
  public function __call($method, $args) {
    return call_user_func_array([$this->subject, $method], $args);
  }

  /**
   * Implements the magic method for getting object properties.
   */
  public function &__get($name) {
    $value = &$this->subject->{$name};
    return $value;
  }

  /**
   * Implements the magic method for setting object properties.
   */
  public function __set($name, $value) {
    $this->subject->{$name} = $value;
  }

  /**
   * Implements the magic method for isset().
   */
  public function __isset($name) {
    return isset($this->subject->{$name});
  }

  /**
   * Implements the magic method for unset().
   */
  public function __unset($name) {
    unset($this->subject->{$name});
  }

  /**
   * Gets the decorated entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The decorated entity.
   */
  public function getEntity() {
    return $this->subject;
  }

  /**
   * {@inheritdoc}
   */
  public function access($operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    return $this->subject->access($operation, $account, $return_as_object);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return $this->subject->getCacheContexts();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    return $this->subject->getCacheTags();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return $this->subject->getCacheMaxAge();
  }

  /**
   * {@inheritdoc}
   */
  public function uuid() {
    return $this->subject->uuid();
  }

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->subject->id();
  }

  /**
   * {@inheritdoc}
   */
  public function language() {
    return $this->subject->language();
  }

  /**
   * {@inheritdoc}
   */
  public function isNew() {
    return $this->subject->isNew();
  }

  /**
   * {@inheritdoc}
   */
  public function enforceIsNew($value = TRUE) {
    $this->subject->enforceIsNew($value);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityTypeId() {
    return $this->subject->getEntityTypeId();
  }

  /**
   * {@inheritdoc}
   */
  public function bundle() {
    return $this->subject->bundle();
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->subject->label();
  }

  /**
   * {@inheritdoc}
   */
  public function urlInfo($rel = 'canonical', array $options = []) {
    return $this->subject->urlInfo($rel, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function toUrl($rel = 'canonical', array $options = []) {
    return $this->subject->toUrl($rel, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function url($rel = 'canonical', $options = []) {
    return $this->subject->url($rel, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function link($text = NULL, $rel = 'canonical', array $options = []) {
    return $this->subject->link($text, $rel, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function toLink($text = NULL, $rel = 'canonical', array $options = []) {
    return $this->subject->toLink($text, $rel, $options);
  }

  /**
   * {@inheritdoc}
   */
  public function hasLinkTemplate($key) {
    return $this->subject->hasLinkTemplate($key);
  }

  /**
   * {@inheritdoc}
   */
  public function uriRelationships() {
    return $this->subject->uriRelationships();
  }

  /**
   * {@inheritdoc}
   */
  public static function load($id) {

  }

  /**
   * {@inheritdoc}
   */
  public static function loadMultiple(array $ids = NULL) {

  }

  /**
   * {@inheritdoc}
   */
  public static function create(array $values = []) {

  }

  /**
   * {@inheritdoc}
   */
  public function save() {
    return $this->subject->save();
  }

  /**
   * {@inheritdoc}
   */
  public function delete() {
    return $this->subject->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    return $this->subject->preSave($storage);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    return $this->subject->postSave($storage, $update);
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {

  }

  /**
   * {@inheritdoc}
   */
  public function postCreate(EntityStorageInterface $storage) {
    return $this->subject->postCreate($storage);
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {

  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {

  }

  /**
   * {@inheritdoc}
   */
  public static function postLoad(EntityStorageInterface $storage, array &$entities) {

  }

  /**
   * {@inheritdoc}
   */
  public function createDuplicate() {
    return $this->subject->createDuplicate();
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityType() {
    return $this->subject->getEntityType();
  }

  /**
   * {@inheritdoc}
   */
  public function referencedEntities() {
    return $this->subject->referencedEntities();
  }

  /**
   * {@inheritdoc}
   */
  public function getOriginalId() {
    return $this->subject->getOriginalId();
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTagsToInvalidate() {
    return $this->subject->getCacheTagsToInvalidate();
  }

  /**
   * {@inheritdoc}
   */
  public function setOriginalId($id) {
    $this->subject->setOriginalId($id);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getTypedData() {
    return $this->subject->getTypedData();
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigDependencyKey() {
    return $this->subject->getConfigDependencyKey();
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigDependencyName() {
    return $this->subject->getConfigDependencyName();
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigTarget() {
    return $this->subject->getConfigTarget();
  }

  /**
   * {@inheritdoc}
   */
  public function toArray() {
    return $this->subject->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function addCacheContexts(array $cache_contexts) {
    $this->subject->addCacheContexts($cache_contexts);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addCacheTags(array $cache_tags) {
    $this->subject->addCacheTags($cache_tags);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function mergeCacheMaxAge($max_age) {
    $this->subject->mergeCacheMaxAge($max_age);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addCacheableDependency($other_object) {
    $this->subject->addCacheableDependency($other_object);
    return $this;
  }

}
