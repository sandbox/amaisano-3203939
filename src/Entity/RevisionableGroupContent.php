<?php

namespace Drupal\group_content_revisions\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionLogEntityTrait;
use Drupal\user\EntityOwnerTrait;

/**
 * Make group content revisionable.
 *
 * Adds required traits/methods for object instances so that we can perform
 * revision-specific operations on the group_content entities.
 */
class RevisionableGroupContent extends EntityDecorator {

  /**
   * Our wrapper's entity type ID (name).
   *
   * @var string
   */
  static public $entityType = 'revisionable_group_content';

  // Graft RevisionableInterface methods onto group_content entities:
  use RevisionLogEntityTrait;
  use EntityOwnerTrait;

  // EntityInterface Static Methods which need to be decorated with entity type
  // inheritance to follow (required).

  /**
   * {@inheritDoc}
   */
  public static function load($id) {
    $entityType = self::$entityType;
    $entityType::load($id);
  }

  /**
   * {@inheritdoc}
   */
  public static function loadMultiple(array $ids = NULL) {
    $entityType = self::$entityType;
    $entityType::loadMultiple($ids);
  }

  /**
   * {@inheritdoc}
   */
  public static function loadByContentPluginId($plugin_id) {
    $entityType = self::$entityType;
    $entityType::loadByContentPluginId($plugin_id);
  }

  /**
   * {@inheritdoc}
   */
  public static function loadByEntity(ContentEntityInterface $entity) {
    $entityType = self::$entityType;
    $entityType::loadByEntity($entity);
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $entityType = self::$entityType;
    $entityType::baseFieldDefinitions($entity_type);
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(
    EntityTypeInterface $entity_type,
    $bundle,
    array $base_field_definitions) {
    $entityType = self::$entityType;
    $entityType::bundleFieldDefinitions(
      $entity_type,
      $bundle,
      $base_field_definitions
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function create(array $values = []) {
    $entityType = self::$entityType;
    $entityType::create($values);
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values) {
    $entityType = self::$entityType;
    $entityType::preCreate($storage, $values);
  }

  /**
   * {@inheritdoc}
   */
  public static function preDelete(EntityStorageInterface $storage, array $entities) {
    $entityType = self::$entityType;
    $entityType::preDelete($storage, $entities);
  }

  /**
   * {@inheritdoc}
   */
  public static function postDelete(EntityStorageInterface $storage, array $entities) {
    $entityType = self::$entityType;
    $entityType::postDelete($storage, $entities);
  }

  /**
   * {@inheritdoc}
   */
  public static function postLoad(EntityStorageInterface $storage, array &$entities) {
    $entityType = self::$entityType;
    $entityType::postLoad($storage, $entities);
  }

  // New/custom methods below (optional).
}
