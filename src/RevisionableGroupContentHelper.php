<?php

namespace Drupal\group_content_revisions;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides helper methods to avoid redundancy.
 */
class RevisionableGroupContentHelper {

  use StringTranslationTrait;

  /**
   * The entity type we are modifying.
   *
   * @var string
   */
  protected $entityTypeId;

  /**
   * Constructor.
   *
   * @param string $entity_type_id
   *   The entity type we wish to modify.
   */
  public function __construct($entity_type_id) {
    $this->entityTypeId = $entity_type_id;
  }

  /**
   * Returns all the database/schema fields needed for revisioning.
   *
   * @param array $field_storage_definitions
   *   Optional: existing base field definitions of entity.
   *
   * @return array
   *   A new or combined array of all base field definitions.
   */
  public function baseFieldDefinitions(array $field_storage_definitions = []) {

    $field_storage_definitions['vid'] = BaseFieldDefinition::create('integer')
      ->setName('revision_id')
      ->setTargetEntityTypeId($this->entityTypeId)
      ->setTargetBundle(NULL)
      ->setLabel($this->t('Revision ID'))
      ->setReadOnly(TRUE)
      ->setSetting('unsigned', TRUE);

    $field_storage_definitions['revision_timestamp'] = BaseFieldDefinition::create('created')
      ->setName('revision_timestamp')
      ->setTargetEntityTypeId($this->entityTypeId)
      ->setTargetBundle(NULL)
      ->setLabel($this->t('Revision create time'))
      ->setDescription($this->t('The time that the current revision was created.'))
      ->setRevisionable(TRUE);

    $field_storage_definitions['revision_uid'] = BaseFieldDefinition::create('entity_reference')
      ->setName('revision_uid')
      ->setTargetEntityTypeId($this->entityTypeId)
      ->setTargetBundle(NULL)
      ->setLabel($this->t('Revision user'))
      ->setDescription($this->t('The user ID of the author of the current revision.'))
      ->setSetting('target_type', 'user')
      ->setRevisionable(TRUE);

    $field_storage_definitions['revision_default'] = BaseFieldDefinition::create('boolean')
      ->setName('revision_default')
      ->setTargetEntityTypeId($this->entityTypeId)
      ->setTargetBundle(NULL)
      ->setLabel($this->t('Default revision'))
      ->setDescription($this->t('A flag indicating whether this was a default revision when it was saved.'))
      ->setStorageRequired(TRUE)
      ->setInternal(TRUE)
      ->setTranslatable(FALSE)
      ->setRevisionable(TRUE);

    $field_storage_definitions['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setName('revision_translation_affected')
      ->setTargetEntityTypeId($this->entityTypeId)
      ->setTargetBundle(NULL)
      ->setLabel($this->t('Revision translation affected'))
      ->setDescription($this->t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    $field_storage_definitions['revision_log'] = BaseFieldDefinition::create('string_long')
      ->setName('revision_log')
      ->setTargetEntityTypeId($this->entityTypeId)
      ->setTargetBundle(NULL)
      ->setLabel($this->t('Revision log message'))
      ->setDescription($this->t('Briefly describe the changes you have made.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue('')
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => 25,
        'settings' => [
          'rows' => 4,
        ],
      ]);

    return $field_storage_definitions;
  }

}
