<?php

namespace Drupal\group_content_revisions\Controller;

use Drupal\diff\Controller\PluginRevisionController;
use Drupal\group\Entity\GroupContentInterface;
use Drupal\group_content_revisions\Entity\RevisionableGroupContent;

/**
 * Extends the diff module controller to get revisions UI.
 */
class GroupContentRevisionsController extends PluginRevisionController {

  /**
   * Generates an overview table of older revisions of a library item.
   *
   * @param \Drupal\group\Entity\GroupContentInterface $group_content
   *   A Group Content entity object.
   *
   * @return array
   *   An array as expected by drupal_render()
   */
  public function revisionOverview(GroupContentInterface $group_content) {
    return $this
      ->formBuilder()
      ->getForm(
      'Drupal\group_content_revisions\Form\GroupContentRevisionsOverviewForm',
      $group_content
    );
  }

  /**
   * Displays a Group Content revision.
   *
   * @param int $group_content_revision
   *   The Group Content revision ID.
   *
   * @return array
   *   A renderable array.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function revisionShow($group_content_revision) {
    $group_content = $this
      ->entityTypeManager()
      ->getStorage('group_content')
      ->loadRevision($group_content_revision);

    $view_builder = $this
      ->entityTypeManager()
      ->getViewBuilder('group_content');

    return $view_builder->view($group_content);
  }

  /**
   * Page title callback for Group Content revision view page.
   *
   * @param int $group_content_revision
   *   The Group Content revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($group_content_revision) {
    /** @var \Drupal\group\Entity\Storage\GroupContentStorageInterface $storage */
    $storage = $this->entityTypeManager()->getStorage('group_content');

    /** @var \Drupal\group\Entity\GroupContentInterface $revision */
    $revision = $storage->loadRevision($group_content_revision);
    $revisionable_group_content = new RevisionableGroupContent($revision);

    // Revision #0 has no data, so use the entity creation time.
    $date = $revisionable_group_content->getRevisionCreationTime() ?? $revision->getChangedTime();

    return $this->t('Revision of %title from %date', [
      '%title' => $revisionable_group_content->label(),
      '%date' => date('m/d/Y - H:i', $date),
    ]);
  }

}
